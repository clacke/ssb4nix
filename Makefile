default: nixes

nixes:
	nix-shell node2nix.nix --run "node2nix -6 --no-flatten --supplement-input supplement.json"

install: nixes
	nix-env -f overrides.nix -iA git-ssb git-ssb-web scuttlebot

.PHONY: nixes install

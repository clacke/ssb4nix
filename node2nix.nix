{ pkgs ? import <nixpkgs> {}
, stdenvNoCC ? pkgs.stdenvNoCC
, fetchgit ? pkgs.fetchgit
, node2nix-nix ? fetchgit {
    url = git://github.com/clacke/node2nix.git;
    rev = "e46d854977a03ad7232297d505bd79c9f20384e4";
    sha256 = "1h4cl23j3m570zn4shh1xf5lpxb71zpfn5r5yqjr0pgid1v16xv7";
  }
, node2nix-attrs ? pkgs.callPackage node2nix-nix {}
, tree ? pkgs.tree
}:

stdenvNoCC.mkDerivation {
  name = "node2nix-env";
  buildInputs = [ node2nix-attrs.package ];
} // { inherit (node2nix-attrs) package shell tarball; }
